import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:work_schedule/data/providers/sqflite/employee_dao.dart';

import 'package:work_schedule/data/providers/sqflite/mixins/soft_delete_sql.dart';
import 'package:work_schedule/data/providers/sqflite/abstract_sqflite_provider.dart';
import 'package:work_schedule/data/providers/sqflite/service_dao.dart';
import 'package:work_schedule/data/providers/sqflite/specialty_dao.dart';

abstract class WorkScheduleSqfliteDao extends AbstractSqfliteProvider with SoftDeleteSql {
  @override
  String get databaseName => 'work_schedule.db';

  @override
  Future onCreate(Database db, int version) async {
    SpecialtyDao specialtyDao = SpecialtyDao();
    await specialtyDao.createTable(db, version);

    EmployeeDao employeeDao = EmployeeDao();
    await employeeDao.createTable(db, version);

    ServiceDao serviceDao = ServiceDao();
    await serviceDao.createTable(db, version);
  }

  @override
  Future onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }
}