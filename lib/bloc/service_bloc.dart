import 'dart:async';

import 'package:work_schedule/data/models/service.dart';
import 'package:work_schedule/data/repository/service_repository.dart';
import 'package:work_schedule/enums/crud_menu_items.dart';

class ServiceBloc {
  //Get instance of the Repository
  static final _serviceRepository = ServiceRepository();

  //Stream controller is the 'Admin' that manages
  //the state of our stream of data like adding
  //new data, change the state of the stream
  //and broadcast it to observers/subscribers
  StreamController<List<Service>> _serviceController = StreamController<List<Service>>.broadcast();
  late StreamSubscription<List<Service>> streamSubscription;

  Stream<List<Service>> _getStreamOfServices({String? name}) {
    return _serviceRepository.getStreamOfServices(name: name).asBroadcastStream();
  }

  Stream<List<Service>> get services => _serviceController.stream;

  ServiceBloc() {
    getServices();
  }

  Future<void> getServices({String? nameLike}) async {
    if (!await _getStreamOfServices(name: nameLike).isEmpty) {
      streamSubscription = _getStreamOfServices(name: nameLike).listen((listOfServicesEvent) {
        if (_serviceController.isClosed) {
          _serviceController = StreamController<List<Service>>.broadcast();
        }

        _serviceController.sink.add(listOfServicesEvent);
      }, onError: (e, _) => print("Error adding listen of Stream of Services to Service controller: $e"));

      return;
    }

    if (_serviceController.isClosed) {
      _serviceController = StreamController<List<Service>>.broadcast();
    }

    //sink is a way of adding data reactively to the stream
    //by registering a new event
    _serviceController.sink.add(await _serviceRepository.getAllServices(nameLike: nameLike));
  }

  Future<void> addService(Service service) async {
    await _serviceRepository.createService(service);
    getServices();
  }

  Future<void> updateService(Service service) async {
    await _serviceRepository.updateService(service);
    getServices();
  }

  Future<void> softDeleteService(int id) async {
    await _serviceRepository.softDeleteService(id);
    getServices();
  }

  Future<void> deleteService(int id) async {
    await _serviceRepository.deleteService(id);
    getServices();
  }

  Future<void> runMethod(CrudMenuItems crudMenuItem, Iterable<Object?>? positionalArguments) async {
    switch (crudMenuItem) {
      case CrudMenuItems.create:
        addService(positionalArguments!.first as Service);
        break;
      case CrudMenuItems.read:
        getServices(nameLike: positionalArguments!.first as String);
        break;
      case CrudMenuItems.edit:
        updateService(positionalArguments!.first as Service);
        break;
      case CrudMenuItems.delete:
        softDeleteService(positionalArguments!.first as int);
        break;
      default:
        throw ArgumentError.value(crudMenuItem, 'name');
    }
  }

  dispose() {
    _serviceController.close();
  }
}
