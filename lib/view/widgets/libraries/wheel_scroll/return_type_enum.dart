enum ReturnType{
  duration('Duration'),
  timeOfDay('TimeOfDay');

  final String type;

  const ReturnType(this.type);
}