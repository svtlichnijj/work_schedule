import 'package:flutter/material.dart';

class WheelWidget extends StatelessWidget {
  final String text;
  final double fontSize;

  static const double fontSizeBig = 20;
  static const double fontSizeNormal = 16;
  static const double fontSizeSmall = 14;

  const WheelWidget(this.text, { Key? key, this.fontSize = fontSizeBig }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
        ),
      ),
    );
  }
}
