import 'package:flutter/material.dart';
import 'package:work_schedule/data/models/service.dart';
import 'package:work_schedule/data/models/model.dart';
import 'package:work_schedule/view/widgets/service_add_widget.dart';

class ServiceAddPage extends StatefulWidget {
  final Service? service;

  const ServiceAddPage({Key? key, required this.service}) : super(key: key);

  @override
  State<ServiceAddPage> createState() => _ServiceAddPageState();
}

class _ServiceAddPageState extends State<ServiceAddPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(!(widget.service?.id != null && widget.service!.id > Model.idForCreating)
            ? 'Add service Form'
            : 'Edit service form of ${widget.service?.name}'
        ),
      ),
      body: ServiceAddWidget(service: widget.service),
    );
  }
}
