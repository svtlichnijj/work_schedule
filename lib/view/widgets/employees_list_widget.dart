import 'package:flutter/material.dart';

import 'package:work_schedule/bloc/employee_bloc.dart';
import 'package:work_schedule/data/models/employee.dart';
import 'package:work_schedule/view/pages/employee_add_page.dart';
import 'package:work_schedule/view/pages/employee_page.dart';
import 'package:work_schedule/view/widgets/common/action_yes_no_index_alert_dialog.dart';
import 'package:work_schedule/view/widgets/common/dismissible_background_builder.dart';

class EmployeesListWidget extends StatefulWidget {
  const EmployeesListWidget({Key? key}) : super(key: key);

  @override
  State<EmployeesListWidget> createState() => _EmployeesListWidgetState();
}

class _EmployeesListWidgetState extends State<EmployeesListWidget> {
  final EmployeeBloc _employeeBloc = EmployeeBloc();
  double detailsProgress = 0.0;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _employeeBloc.employees,
        builder: (BuildContext context, AsyncSnapshot<List<Employee>> snapshot) {
          return _getEmployeeCardWidget(snapshot);
        });
  }

  Widget _getEmployeeCardWidget(AsyncSnapshot<List<Employee>> snapshot) {
    if (snapshot.hasData) {
      return snapshot.data!.isEmpty
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : RefreshIndicator(
              onRefresh: _employeeBloc.getEmployees,
              strokeWidth: 3.0,
              child: ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  Employee employee = snapshot.data![index];
                  return Dismissible(
                    key: ValueKey<Employee>(employee),
                    onUpdate: (DismissUpdateDetails details) {
                      setState(() {
                        detailsProgress = details.progress;
                      });
                    },
                    background: DismissibleBackgroundBuilder(
                      detailsProgress: detailsProgress,
                      color: Colors.green,
                      children: const <Widget>[
                        Icon(
                          Icons.edit,
                          color: Colors.white,
                        ),
                        Text(
                          'Edit',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    secondaryBackground: DismissibleBackgroundBuilder(
                      detailsProgress: detailsProgress,
                      direction: DismissDirection.endToStart,
                      children: const <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    confirmDismiss: (direction) async {
                      if (direction == DismissDirection.endToStart) {
                        return await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return ActionYesNoIndexAlertDialog(
                                  content:
                                      'Are you sure you want to delete ${employee.name} (${employee.specialty.name})?',
                                  trueText: 'Delete',
                                  callback: (bool isApprove) async {
                                    if (isApprove) {
                                      _employeeBloc.softDeleteEmployee(employee.id);

                                      ScaffoldMessenger.of(context)
                                        ..hideCurrentSnackBar()
                                        ..showSnackBar(SnackBar(
                                          content: Text('You deleted service ${employee.toString()}'),
                                          duration: const Duration(seconds: 5),
                                        ));
                                    }
                                  });
                            });
                      } else {
                        await Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) => EmployeeAddPage(employee: employee)));
                      }

                      return null;
                    },
                    child: Card(
                      child: ListTile(
                        leading: Text(employee.id.toString()),
                        title: Text(
                          employee.name,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(employee.specialty.name),
                        onTap: () async => await Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) => EmployeePage(employeeId: employee.id))),
                      ),
                    ),
                  );
                },
              ),
            );
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            CircularProgressIndicator(),
            Text(
              'Loading...',
              style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
            ),
          ],
        ),
      );
    }
  }

  @override
  dispose() {
    _employeeBloc.dispose();
    super.dispose();
  }
}
