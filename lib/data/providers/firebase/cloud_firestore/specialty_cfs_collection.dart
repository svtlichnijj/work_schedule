import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/mixins/soft_delete_firestore.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/work_schedule_firestore_collection.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';

class SpecialtyCfsCollection extends WorkScheduleFirestoreCollection {
  static String get collectionName => 'specialties';

  final CollectionReference<Map<String, dynamic>> _specialtyCollection =
      FirebaseFirestore.instance.collection(collectionName);

  List<Map<String, dynamic>> _getSpecialtiesCollection() {
    return [
      {SpecialtyRepository.columnId: 1, SpecialtyRepository.columnName: 'Beautician'},
      {SpecialtyRepository.columnId: 2, SpecialtyRepository.columnName: 'Manicure'},
      {SpecialtyRepository.columnId: 3, SpecialtyRepository.columnName: 'Masseur'},
    ];
  }

  Future<void> fillSpecialties() async {
    final QuerySnapshot<Map<String, dynamic>> query = await _specialtyCollection.get();

    if (query.docs.isEmpty) {
      Set<Specialty> specialties = {};
      _getSpecialtiesCollection().forEach((Map<String, dynamic> employeeMap) {
        specialties.add(Specialty.fromMap(employeeMap));
      });

      await insertBatchSpecialties(specialties);
    }
  }

  Future<String> insertSpecialty(Specialty specialty) async {
    String specialtyKey = toSnakeCase(specialty.name);
    _specialtyCollection
        .doc(specialtyKey)
        .set(mergeDefaultDeleteAtColumn(specialty.toMap()))
        .onError((e, _) => print("Error inserting Specialty document: $e"));

    return specialtyKey;
  }

  Future<void> insertBatchSpecialties(Set<Specialty> specialties) async {
    final batch = FirebaseFirestore.instance.batch();

    for (Specialty specialty in specialties) {
      DocumentReference specialtyRef = _specialtyCollection.doc(toSnakeCase(specialty.name));
      batch.set(specialtyRef, mergeDefaultDeleteAtColumn(specialty.toMap()));
    }

    batch.commit().then((_) {});
  }

  Query<Map<String, dynamic>> prepareSpecialtyCollectionQuery(
      {String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) {
    Query<Map<String, dynamic>> specialtyCollection = _specialtyCollection;
    specialtyCollection = onWhereNotDeleted(specialtyCollection);

    if (name != null) {
      specialtyCollection = specialtyCollection.where('name', isEqualTo: name);
    }

    if (limitIn != null) {
      specialtyCollection = specialtyCollection.limit(limitIn);
    }

    if (orderByIn != null) {
      List<String> orderByValues = orderByIn.split(' ');
      String orderBy = orderByValues[0];
      bool orderDescending = orderByValues[1].toLowerCase() == 'desc';
      specialtyCollection = specialtyCollection.orderBy(orderBy, descending: orderDescending);
    }

    return specialtyCollection;
  }

  Future<List<Specialty>> getSpecialties({String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) async {
    Query<Map<String, dynamic>> specialtyCollection =
        prepareSpecialtyCollectionQuery(name: name, limitIn: limitIn, orderByIn: orderByIn);

    return await specialtyCollection.get().then((QuerySnapshot<Map<String, dynamic>> specialtySnapshot) =>
        specialtySnapshot
            .docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> specialtyDocumentSnapshot) =>
                Specialty.fromMap(specialtyDocumentSnapshot.data()))
            .toList());
  }

  Stream<List<Specialty>> getStreamOfSpecialties(
      {String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) async* {
    Query<Map<String, dynamic>> specialtyStreamCollection =
        prepareSpecialtyCollectionQuery(name: name, limitIn: limitIn, orderByIn: orderByIn);

    Stream<QuerySnapshot<Map<String, dynamic>>> specialtySnapshots = specialtyStreamCollection.snapshots();

    yield* specialtySnapshots.map((QuerySnapshot<Map<String, dynamic>> specialtySnapshot) => specialtySnapshot.docs
        .map((QueryDocumentSnapshot<Map<String, dynamic>> specialtyDocumentSnapshot) =>
            Specialty.fromMap(specialtyDocumentSnapshot.data()))
        .toList());
  }

  Future<Specialty> getSpecialty(int specialtyId) async {
    Query<Map<String, dynamic>> specialtyCollection = prepareSpecialtyCollectionQuery(limitIn: 1, orderByIn: null);
    specialtyCollection = specialtyCollection.where('id', isEqualTo: specialtyId);

    return await specialtyCollection.get().then((QuerySnapshot<Map<String, dynamic>> specialtySnapshot) =>
        specialtySnapshot
            .docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> specialtyDocumentSnapshot) =>
                Specialty.fromMap(specialtyDocumentSnapshot.data()))
            .toList()
            .first);
  }

  Future<Specialty> updateSpecialty(Specialty specialty) async {
    final String docId = await _specialtyCollection
        .where(SpecialtyRepository.columnId, isEqualTo: specialty.id)
        .get()
        .then((specialtySnapshot) => specialtySnapshot.docs.first.id);
    _specialtyCollection
        .doc(docId)
        .update(specialty.toMap())
        .onError((e, _) => print("Error update Specialty document: $e"));

    return specialty;
  }

  softDeleteSpecialty(int specialtyId) async {
    final QuerySnapshot specialtySnapshot =
        await _specialtyCollection.where(SpecialtyRepository.columnId, isEqualTo: specialtyId).limit(1).get();

    if (specialtySnapshot.docs.isNotEmpty) {
      _specialtyCollection
          .doc(toSnakeCase(specialtySnapshot.docs.first.id))
          .update({SoftDeleteFirestore.columnDeletedAt: Timestamp.now()}).onError(
              (e, _) => print("Error soft delete Specialty document: $e"));
    }
  }

  deleteSpecialty(int specialtyId) async {
    final QuerySnapshot specialtySnapshot =
        await _specialtyCollection.where(SpecialtyRepository.columnId, isEqualTo: specialtyId).limit(1).get();

    if (specialtySnapshot.docs.isNotEmpty) {
      _specialtyCollection
          .doc(toSnakeCase(specialtySnapshot.docs.first.id))
          .delete()
          .onError((e, _) => print("Error delete Specialty document: $e"));
    }
  }
}
