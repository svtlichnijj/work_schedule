import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class IdScrollNotification extends ScrollNotification {
  final String id;
  final ScrollDirection direction;

  IdScrollNotification({ required this.id, required super.metrics, required super.context, required this.direction });
}