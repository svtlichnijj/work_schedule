import 'package:work_schedule/data/models/model.dart';
import 'package:work_schedule/data/repository/service_repository.dart';

class Service extends Model {
  int _id = Model.idForCreating;

  @override
  get id => _id;
  String name;
  Duration duration;

  Service({ required this.name, required this.duration });

  factory Service.fromMap(Map<String, dynamic> map) {
    Service service = Service(
      name: map[ServiceRepository.columnName],
      duration: Duration(seconds: map[ServiceRepository.columnDuration]),
    );
    service._id = map[ServiceRepository.columnId];

    return service;
  }

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      ServiceRepository.columnName: name,
      ServiceRepository.columnDuration: duration.inSeconds,
    };

    if (_id != Model.idForCreating) {
      map[ServiceRepository.columnId] = _id;
    }

    return map;
  }

  bool isEqualsServices(Service other) {
    return identical(this, other)
        || runtimeType == other.runtimeType
            && _id == other.id
            && name == other.name
            && duration == other.duration;
  }

  @override
  String toString() {
    return '$name (${duration.toString().split('.').first.padLeft(8, '0')})';
  }
}
