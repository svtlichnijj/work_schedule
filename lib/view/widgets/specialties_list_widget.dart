import 'package:flutter/material.dart';

import 'package:work_schedule/bloc/specialty_bloc.dart';
import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';
import 'package:work_schedule/view/widgets/common/action_yes_no_index_alert_dialog.dart';
import 'package:work_schedule/view/widgets/common/dismissible_background_builder.dart';
import 'package:work_schedule/view/widgets/common/edit_text_alert_dialog.dart';

class SpecialtiesListWidget extends StatefulWidget {
  final SpecialtyBloc _specialtyBloc;

  const SpecialtiesListWidget(this._specialtyBloc, {Key? key}) : super(key: key);

  @override
  State<SpecialtiesListWidget> createState() => _SpecialtiesListWidgetState();
}

class _SpecialtiesListWidgetState extends State<SpecialtiesListWidget> {
  SpecialtyRepository specialtyRepository = SpecialtyRepository();
  double detailsProgress = 0.0;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: widget._specialtyBloc.specialties,
        builder: (BuildContext context, AsyncSnapshot<List<Specialty>> snapshot) {
          return _getSpecialtyCardWidget(snapshot);
        });
  }

  Widget _getSpecialtyCardWidget(AsyncSnapshot<List<Specialty>> snapshot) {
    if (snapshot.hasData) {
      return snapshot.data!.isEmpty
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : RefreshIndicator(
              onRefresh: widget._specialtyBloc.getSpecialties,
              strokeWidth: 3.0,
              child: ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (BuildContext context, int index) {
                  Specialty specialty = snapshot.data![index];
                  return Dismissible(
                    key: ValueKey<Specialty>(specialty),
                    onUpdate: (DismissUpdateDetails details) {
                      setState(() {
                        detailsProgress = details.progress;
                      });
                    },
                    background: DismissibleBackgroundBuilder(
                      detailsProgress: detailsProgress,
                      children: const <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    secondaryBackground: DismissibleBackgroundBuilder(
                      detailsProgress: detailsProgress,
                      direction: DismissDirection.endToStart,
                      children: const <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    confirmDismiss: (direction) async {
                      return await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return ActionYesNoIndexAlertDialog(
                              content: 'Are you sure you want to delete specialty ${specialty.name}?',
                              trueText: 'Delete',
                              callback: (bool isApprove) async {
                                if (isApprove) {
                                  widget._specialtyBloc.softDeleteSpecialty(specialty.id);
                                }
                              },
                            );
                          });
                    },
                    child: Card(
                      child: ListTile(
                        leading: Text(specialty.id.toString()),
                        title: Text(
                          specialty.name,
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        trailing: const Icon(
                          Icons.drive_file_rename_outline,
                        ),
                        onTap: () {
                          showDialog<String>(
                              context: context,
                              builder: (BuildContext context) {
                                return EditTextAlertDialog(
                                    title: 'Rename specialty',
                                    label: 'Specialty',
                                    textIn: specialty.name,
                                    submitText: 'Edit',
                                    callback: (text) {
                                      if (specialty.name != text) {
                                        specialty.name = text;
                                        widget._specialtyBloc.updateSpecialty(specialty);
                                      }
                                    });
                              });
                        },
                      ),
                    ),
                  );
                },
              ),
            );
    } else {
      return Center(
        child: loadingData(),
      );
    }
  }

  Widget loadingData() {
    widget._specialtyBloc.getSpecialties();

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[
          CircularProgressIndicator(),
          Text(
            'Loading...',
            style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}
