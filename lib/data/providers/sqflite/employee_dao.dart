import 'package:sqflite/sqflite.dart';

import 'package:work_schedule/data/models/employee.dart';
import 'package:work_schedule/data/providers/sqflite/mixins/soft_delete_sql.dart';
import 'package:work_schedule/data/providers/sqflite/specialty_dao.dart';
import 'package:work_schedule/data/providers/sqflite/work_schedule_sqflite_dao.dart';
import 'package:work_schedule/data/repository/employee_repository.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';

class EmployeeDao extends WorkScheduleSqfliteDao {
  @override
  String get tableName => 'employees_table';

  static final EmployeeDao _instance = EmployeeDao._();

  factory EmployeeDao() {
    return _instance;
  }

  EmployeeDao._();

  List<Map<String, dynamic>> _getEmployeesMap() {
    return [
      {
        'employee.id': 1,
        'employee.name': 'Ann',
        'employee.specialty_id': 1,
        'specialty.id': 1,
        'specialty.name': 'Beautician'
      },
      {
        'employee.id': 2,
        'employee.name': 'Lisa',
        'employee.specialty_id': 2,
        'specialty.id': 2,
        'specialty.name': 'Manicure'
      },
      {
        'employee.id': 3,
        'employee.name': 'Sony',
        'employee.specialty_id': 3,
        'specialty.id': 3,
        'specialty.name': 'Masseur'
      },
      {
        'employee.id': 4,
        'employee.name': 'Ann2',
        'employee.specialty_id': 3,
        'specialty.id': 3,
        'specialty.name': 'Masseur'
      },
      {
        'employee.id': 5,
        'employee.name': 'Lisa2',
        'employee.specialty_id': 1,
        'specialty.id': 1,
        'specialty.name': 'Beautician'
      },
      {
        'employee.id': 6,
        'employee.name': 'Sony2',
        'employee.specialty_id': 2,
        'specialty.id': 2,
        'specialty.name': 'Manicure'
      },
      {
        'employee.id': 7,
        'employee.name': 'Ann3',
        'employee.specialty_id': 2,
        'specialty.id': 2,
        'specialty.name': 'Manicure'
      },
      {
        'employee.id': 8,
        'employee.name': 'Lisa3',
        'employee.specialty_id': 3,
        'specialty.id': 3,
        'specialty.name': 'Masseur'
      },
      {
        'employee.id': 9,
        'employee.name': 'Sony3',
        'employee.specialty_id': 1,
        'specialty.id': 1,
        'specialty.name': 'Beautician'
      },
    ];
  }

  Future createTable(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableName (
        ${EmployeeRepository.columnId} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${EmployeeRepository.columnName} TEXT NOT NULL,
        ${EmployeeRepository.columnSpecialtyId} INTEGER NOT NULL,
        $rowCreateSoftDeleteColumn,
        FOREIGN KEY (${EmployeeRepository.columnSpecialtyId}) REFERENCES ${SpecialtyDao().tableName} (${SpecialtyRepository.columnId}) 
          ON DELETE NO ACTION ON UPDATE NO ACTION
      );
    ''');

    await fillEmployees(databaseIn: db);
  }

  Future<List<List<Object?>>> fillEmployees({Database? databaseIn}) async {
    Set<Employee> employees = {};
    _getEmployeesMap().forEach((Map<String, dynamic> employeeMap) {
      employees.add(Employee.fromMapWithSpecialty(employeeMap));
    });
    List<List<Object?>> result = await insertBatchEmployees(employees, databaseIn: databaseIn);

    return result;
  }

  Future<int> insertEmployee(Employee employee) async {
    Database db = await _instance.database;
    return await db.insert(tableName, employee.toMap());
  }

  Future<List<List<Object?>>> insertBatchEmployees(Set<Employee> employees, {Database? databaseIn}) async {
    Database db = databaseIn ?? await _instance.database;
    List<List<Object?>> results = [];
    db.transaction((txn) async {
      Batch batch = txn.batch();
      for (Employee employee in employees) {
        batch.insert(tableName, employee.toMap());
      }

      results.add(await batch.commit(noResult: false));
    });

    return results;
  }

  Future<List<Employee>> getEmployees({String? nameLike, int? limitIn = 100, String? orderByIn = 'id DESC'}) async {
    Database db = await _instance.database;
    final List<Map<String, dynamic>> result;

    if (nameLike != null && nameLike.isNotEmpty) {
      result = await db.query(
        tableName,
        where: '${SoftDeleteSql.onWhereNotDeleted()} AND ${EmployeeRepository.columnName} LIKE ?',
        whereArgs: ["%$nameLike%"],
        limit: limitIn,
        orderBy: orderByIn,
      );
    } else {
      result = await db.query(
        tableName,
        where: SoftDeleteSql.onWhereNotDeleted(),
        limit: limitIn,
        orderBy: orderByIn,
      );
    }

    return List.generate(result.length, (i) => Employee.fromMap(result[i]));
  }

  String _querySelectWithSpecialties() {
    EmployeeRepository employeeRepository = EmployeeRepository();
    SpecialtyRepository specialtyRepository = SpecialtyRepository();

    return '''
      SELECT 
        ${employeeRepository.joinAlias}.id AS '${employeeRepository.joinAlias}.id',
        ${employeeRepository.joinAlias}.name AS '${employeeRepository.joinAlias}.name',
        ${employeeRepository.joinAlias}.specialty_id AS '${employeeRepository.joinAlias}.specialty_id',
        ${specialtyRepository.joinAlias}.id AS '${specialtyRepository.joinAlias}.id',
        ${specialtyRepository.joinAlias}.name AS '${specialtyRepository.joinAlias}.name'
        FROM $tableName AS ${employeeRepository.joinAlias}
      INNER JOIN ${SpecialtyDao().tableName} as ${specialtyRepository.joinAlias}
        ON ${employeeRepository.joinAlias}.${employeeRepository.foreignKeyColumnName} = ${specialtyRepository.joinAlias}.${specialtyRepository.innerKeyColumnName}
    ''';
  }

  Future<List<Employee>> fetchEmployeesWithSpecialties({String? nameLike}) async {
    Database db = await _instance.database;
    String rawQuery = '''
      ${_querySelectWithSpecialties()}
      WHERE ${SoftDeleteSql.onWhereNotDeleted(alias: EmployeeRepository().joinAlias)}
    ''';
    List arguments = [];

    if (nameLike != null && nameLike.isNotEmpty) {
      rawQuery += ''' AND ${EmployeeRepository.columnName} LIKE ?
      ''';
      arguments.add(nameLike);
    }

    final List<Map<String, dynamic>> maps = await db.rawQuery(rawQuery, arguments);

    return List.generate(maps.length, (i) => Employee.fromMapWithSpecialty(maps[i]));
  }

  Future<Employee> getEmployee(int employeeId) async {
    Database db = await _instance.database;
    final List<Map<String, dynamic>> map = await db.query(
      tableName,
      where: '${SoftDeleteSql.onWhereNotDeleted()} = ? AND ${EmployeeRepository.columnId}',
      whereArgs: [employeeId],
    );

    return Employee.fromMap(map.last);
  }

  Future<Employee> getEmployeeWithSpecialty(int employeeId) async {
    Database db = await _instance.database;
    String rawQuery = '''
      ${_querySelectWithSpecialties()}
      WHERE ${SoftDeleteSql.onWhereNotDeleted(alias: EmployeeRepository().joinAlias)} 
        AND ${EmployeeRepository().joinAlias}.${EmployeeRepository.columnId} = ?
    ''';
    final List<Map<String, dynamic>> map = await db.rawQuery(rawQuery, [employeeId]);

    return Employee.fromMapWithSpecialty(map.last);
  }

  Future<int?> employeesCount() async {
    Database db = await _instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $tableName'));
  }

  Future<int> updateEmployee(Employee employee) async {
    Database db = await _instance.database;

    return await db.update(
      tableName,
      employee.toMap(),
      where: '${EmployeeRepository.columnId} = ?',
      whereArgs: [employee.id],
    );
  }

  Future<int> softDeleteEmployee(int employeeId) async {
    Database db = await _instance.database;
    return await db.update(tableName, {SoftDeleteSql.columnDeletedAt: DateTime.now().millisecondsSinceEpoch},
        where: '${EmployeeRepository.columnId} = ?', whereArgs: [employeeId]);
  }

  Future<int> deleteEmployee(int employeeId) async {
    Database db = await _instance.database;
    return await db.delete(tableName, where: '${EmployeeRepository.columnId} = ?', whereArgs: [employeeId]);
  }
}
