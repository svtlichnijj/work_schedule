import 'package:flutter/material.dart';

import './wheel_widget.dart';

class WheelHours extends WheelWidget {
  final int _hours;

  WheelHours(this._hours, { super.key }) : super(_hours.toString());

  @override
  Widget build(BuildContext context) {
    return WheelWidget(_hours.toString());
  }

  static List<Widget> getList({ bool isAmPm = false }) {
    return List<Widget>.generate(
        isAmPm ? 12 : 24,
        (int index) => WheelHours(index)
    );
  }
}
