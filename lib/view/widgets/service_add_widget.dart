import 'package:flutter/material.dart';
import 'package:work_schedule/bloc/service_bloc.dart';

import 'package:work_schedule/data/models/model.dart';
import 'package:work_schedule/data/models/service.dart';
import 'package:work_schedule/enums/crud_menu_items.dart';
import 'package:work_schedule/view/widgets/common/action_yes_no_index_alert_dialog.dart';
import 'package:work_schedule/view/widgets/libraries/wheel_scroll/wheels_scroll_widget.dart';

class ServiceAddWidget extends StatefulWidget {
  final Service? service;

  const ServiceAddWidget({ Key? key, required this.service }) : super(key: key);

  @override
  State<ServiceAddWidget> createState() => _ServiceAddWidgetState();
}

class _ServiceAddWidgetState extends State<ServiceAddWidget> {
  final _formKey = GlobalKey<FormState>();
  late final bool _isAddingService;
  late final Service _serviceEntering;
  late Service _serviceUpserting;
  final ServiceBloc _serviceBloc = ServiceBloc();

  @override
  void initState() {
    super.initState();

    _fillServices();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        onWillPop: () async {
          bool? shouldPop = _serviceUpserting.isEqualsServices(_serviceEntering);

          if (!shouldPop) {
            shouldPop = await showDialog<bool>(
              context: context,
              builder: (context) {
                String dialogTitle = 'creation';
                String dialogContext = 'the continuation of the creation of the Service';

                if (!_isAddingService) {
                  dialogTitle = 'editing';
                  dialogContext = 'continuing to edit ${_serviceUpserting.toString()})';
                }

                return ActionYesNoIndexAlertDialog(
                    title: 'Cancel $dialogTitle',
                    content: 'Are you sure you want to cancel $dialogContext ?',
                    falseText: 'Stay',
                    trueText: 'Leave',
                    callback: (isApprove) => {}
                );
              },
            );
          }

          return shouldPop!;
        },
        child: Column(
          children: <Widget>[
            Expanded(
                child: ListView(
                  shrinkWrap: true,
                    children: [
                      TextFormField(
                        initialValue: _serviceUpserting.name,
                        decoration: const InputDecoration(
                          label: Text('Name'),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter name of the service';
                          }

                          return null;
                        },
                        onChanged: (String? value) {
                          setState(() {
                            _serviceUpserting.name = value!;
                          });
                        },
                        onSaved: (String? value) {
                          _serviceUpserting.name = value!;
                        },
                      ),
                      WheelsScrollWidget.callbackDuration(
                        isShowCaptions: true,
                        onWheelScrolledDuration: (Duration duration) {
                          _serviceUpserting.duration = duration;
                        },
                        initialTimerDuration: _serviceUpserting.duration,
                      ),
                    ],
                )
            ),
            ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState?.save();
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Processing Data')));
                  _upsertService(context);
                }
              },
              child: Text(
                _isAddingService ? 'Add service' : 'Edit service',
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _fillServices() async {
    _isAddingService = !(widget.service?.id != null && widget.service!.id > Model.idForCreating);

    if (_isAddingService) {
      _serviceEntering = Service(name: '', duration: const Duration());
      _serviceUpserting = Service(name: '', duration: const Duration());
    } else {
      _serviceEntering = Service.fromMap(widget.service!.toMap());
      _serviceUpserting = Service.fromMap(widget.service!.toMap());
    }
  }

  _upsertService(BuildContext context) {
    String successSnackMessage = '';

    if (_isAddingService) {
      _serviceBloc.runMethod(CrudMenuItems.create, [_serviceUpserting]);
      successSnackMessage = 'Created Service ${_serviceUpserting.toString()}';
    } else {
      _serviceBloc.runMethod(CrudMenuItems.edit, [_serviceUpserting]);
      successSnackMessage = 'Edited Service ${_serviceUpserting.name}';
    }

    if (mounted) {
      Navigator.pop(context, _serviceUpserting != _serviceEntering);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(successSnackMessage)));
    }
  }

  @override
  dispose() {
    _serviceBloc.dispose();
    super.dispose();
  }
}
