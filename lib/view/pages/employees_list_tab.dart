import 'package:flutter/material.dart';
import 'package:work_schedule/view/pages/employee_add_page.dart';
import 'package:work_schedule/view/widgets/employees_list_widget.dart';

class EmployeesListTab extends StatefulWidget {
  const EmployeesListTab({Key? key}) : super(key: key);

  @override
  State<EmployeesListTab> createState() => _EmployeesListTabState();
}

class _EmployeesListTabState extends State<EmployeesListTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const EmployeesListWidget(),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () async {
          await Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => const EmployeeAddPage(employee: null)));
        },
      ),
    );
  }
}
