import 'package:work_schedule/data/models/service.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/service_cfs_collection.dart';
import 'package:work_schedule/data/providers/sqflite/service_dao.dart';

class ServiceRepository {
  static const columnId = 'id';
  static const columnName = 'name';
  static const columnDuration = 'duration';

  final _serviceDao = ServiceDao();
  final _serviceCfsCollection = ServiceCfsCollection();

  Stream<List<Service>> getStreamOfServices({String? name}) {
    return _serviceCfsCollection.getStreamOfServices(name: name);
  }

  Future<List<Service>> getAllServices({String? nameLike}) async {
    return await _serviceCfsCollection.getServices(name: nameLike);
    // return _serviceDao.getServices(nameLike: nameLike);
  }

  Future<String> createService(Service service) async {
    return await _serviceCfsCollection.insertService(service);
    // return _serviceDao.insertService(service);
  }

  Future<String> updateService(Service service) async {
    return await _serviceCfsCollection.updateService(service);
    // return _serviceDao.updateService(service);
  }

  Future<void> softDeleteService(int serviceId) async {
    return _serviceCfsCollection.softDeleteService(serviceId);
    // return await _serviceDao.softDeleteService(serviceId);
  }

  Future<void> deleteService(int serviceId) async {
    return await _serviceCfsCollection.deleteService(serviceId);
    // return await _serviceDao.deleteService(serviceId);
  }
}
