import 'package:flutter/material.dart';

import './wheel_widget.dart';

class WheelSeconds extends WheelWidget {
  final int _seconds;

  WheelSeconds(this._seconds, { super.key }) : super(_seconds.toString());

  @override
  Widget build(BuildContext context) {
    return WheelWidget(_seconds.toString());
  }

  static List<Widget> getList() {
    return List<Widget>.generate(
        60,
        (int index) => WheelSeconds(index)
    );
  }
}
