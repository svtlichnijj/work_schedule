import 'package:work_schedule/data/providers/firebase/cloud_firestore/employee_cfs_collection.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/mixins/soft_delete_firestore.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/service_cfs_collection.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/specialty_cfs_collection.dart';

class WorkScheduleFirestoreCollection with SoftDeleteFirestore {
  Future onCreate() async {
    await ServiceCfsCollection().fillServices();
    await SpecialtyCfsCollection().fillSpecialties();
    await EmployeeCfsCollection().fillEmployees();
  }

  String toSnakeCase(String stringIn) =>
      stringIn.toLowerCase().replaceAll(RegExp(r'\W+'), ' ').trim().replaceAll(' ', '_');
}
