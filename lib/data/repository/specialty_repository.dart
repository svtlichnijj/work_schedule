import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/specialty_cfs_collection.dart';
import 'package:work_schedule/data/providers/sqflite/specialty_dao.dart';
import 'package:work_schedule/data/repository/mixins/foreign_repository.dart';

class SpecialtyRepository with ForeignRepository {
  static const columnId = 'id';
  static const columnName = 'name';

  @override
  String get joinAlias => 'specialty';

  final SpecialtyDao _specialtyDao = SpecialtyDao();
  final SpecialtyCfsCollection _specialtyCfsCollection = SpecialtyCfsCollection();

  Stream<List<Specialty>> getStreamOfSpecialties({String? name}) {
    return _specialtyCfsCollection.getStreamOfSpecialties(name: name);
  }

  Future<List<Specialty>> getAllSpecialties({String? nameLike}) async {
    return await _specialtyCfsCollection.getSpecialties(name: nameLike);
    // return await _specialtyDao.getSpecialties(nameLike: nameLike);
  }

  Future<Specialty> getSpecialty(int specialtyId) async {
    return await _specialtyCfsCollection.getSpecialty(specialtyId);
    // return await _specialtyDao.getSpecialty(specialtyId);
  }

  Future<String> createSpecialty(Specialty specialty) async {
    return _specialtyCfsCollection.insertSpecialty(specialty);
    // return await _specialtyDao.upsertSpecialty(specialty);
  }

  Future<Specialty> updateSpecialty(Specialty specialty) async {
    return _specialtyCfsCollection.updateSpecialty(specialty);
    // return await _specialtyDao.upsertSpecialty(specialty);
  }

  Future<void> softDeleteSpecialty(int specialtyId) async {
    return await _specialtyCfsCollection.softDeleteSpecialty(specialtyId);
    // return await _specialtyDao.softDeleteSpecialty(specialtyId);
  }

  Future<void> deleteSpecialty(int specialtyId) async {
    return await _specialtyCfsCollection.deleteSpecialty(specialtyId);
    // return await _specialtyDao.deleteSpecialty(specialtyId);
  }
}
