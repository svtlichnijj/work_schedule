import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:work_schedule/data/models/service.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/mixins/soft_delete_firestore.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/work_schedule_firestore_collection.dart';
import 'package:work_schedule/data/repository/service_repository.dart';

class ServiceCfsCollection extends WorkScheduleFirestoreCollection {
  static String get collectionName => 'services';

  final CollectionReference<Map<String, dynamic>> _serviceCollection =
      FirebaseFirestore.instance.collection(collectionName);

  List<Map<String, dynamic>> _getServicesCollection() {
    return [
      {ServiceRepository.columnId: 1, ServiceRepository.columnName: 'Hour', ServiceRepository.columnDuration: 3600},
      {
        ServiceRepository.columnId: 2,
        ServiceRepository.columnName: 'Half-hour',
        ServiceRepository.columnDuration: 1800
      },
      {
        ServiceRepository.columnId: 3,
        ServiceRepository.columnName: 'Three-hour',
        ServiceRepository.columnDuration: 10800
      },
    ];
  }

  Future<void> fillServices() async {
    final QuerySnapshot<Map<String, dynamic>> query = await _serviceCollection.get();

    if (query.docs.isEmpty) {
      Set<Service> services = {};
      _getServicesCollection().forEach((Map<String, dynamic> employeeMap) {
        services.add(Service.fromMap(employeeMap));
      });

      await insertBatchServices(services);
    }
  }

  Future<String> insertService(Service service) async {
    String serviceKey = toSnakeCase(service.name);
    _serviceCollection
        .doc(serviceKey)
        .set(mergeDefaultDeleteAtColumn(service.toMap()))
        .onError((e, _) => print("Error inserting Service document: $e"));

    return serviceKey;
  }

  Future<void> insertBatchServices(Set<Service> services) async {
    final batch = FirebaseFirestore.instance.batch();

    for (Service service in services) {
      DocumentReference serviceRef = _serviceCollection.doc(toSnakeCase(service.name));
      batch.set(serviceRef, mergeDefaultDeleteAtColumn(service.toMap()));
    }

    batch.commit().then((_) {});
  }

  Query<Map<String, dynamic>> prepareServiceCollectionQuery(
      {String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) {
    Query<Map<String, dynamic>> serviceStreamCollection = _serviceCollection;
    serviceStreamCollection = onWhereNotDeleted(serviceStreamCollection);

    if (name != null) {
      serviceStreamCollection = serviceStreamCollection.where('name', isEqualTo: name);
    }

    if (limitIn != null) {
      serviceStreamCollection = serviceStreamCollection.limit(limitIn);
    }

    if (orderByIn != null) {
      List<String> orderByValues = orderByIn.split(' ');
      String orderBy = orderByValues[0];
      bool orderDescending = orderByValues[1].toLowerCase() == 'desc';
      serviceStreamCollection = serviceStreamCollection.orderBy(orderBy, descending: orderDescending);
    }

    return serviceStreamCollection;
  }

  Future<List<Service>> getServices({String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) async {
    Query<Map<String, dynamic>> serviceStreamCollection =
        prepareServiceCollectionQuery(name: name, limitIn: limitIn, orderByIn: orderByIn);

    return await serviceStreamCollection.get().then((QuerySnapshot<Map<String, dynamic>> serviceSnapshot) =>
        serviceSnapshot
            .docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> serviceDocumentSnapshot) =>
                Service.fromMap(serviceDocumentSnapshot.data()))
            .toList());
  }

  Stream<List<Service>> getStreamOfServices({String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) async* {
    Query<Map<String, dynamic>> serviceStreamCollection =
        prepareServiceCollectionQuery(name: name, limitIn: limitIn, orderByIn: orderByIn);

    Stream<QuerySnapshot<Map<String, dynamic>>> serviceSnapshots = serviceStreamCollection.snapshots();

    yield* serviceSnapshots.map((QuerySnapshot<Map<String, dynamic>> serviceSnapshot) => serviceSnapshot.docs
        .map((QueryDocumentSnapshot<Map<String, dynamic>> serviceDocumentSnapshot) =>
            Service.fromMap(serviceDocumentSnapshot.data()))
        .toList());
  }

  Future<String> updateService(Service service) async {
    final String docId = await _serviceCollection
        .where(ServiceRepository.columnId, isEqualTo: service.id)
        .get()
        .then((serviceSnapshot) => serviceSnapshot.docs.first.id);
    _serviceCollection
        .doc(docId)
        .update(service.toMap())
        .onError((e, _) => print("Error updating Service document: $e"));

    return docId;
  }

  softDeleteService(int serviceId) async {
    final QuerySnapshot serviceSnapshot =
        await _serviceCollection.where(ServiceRepository.columnId, isEqualTo: serviceId).limit(1).get();

    if (serviceSnapshot.docs.isNotEmpty) {
      _serviceCollection
          .doc(toSnakeCase(serviceSnapshot.docs.first.id))
          .update({SoftDeleteFirestore.columnDeletedAt: FieldValue.serverTimestamp()}).onError(
              (e, _) => print("Error soft delete Service document: $e"));
    }
  }

  deleteService(int serviceId) async {
    final QuerySnapshot serviceSnapshot =
        await _serviceCollection.where(ServiceRepository.columnId, isEqualTo: serviceId).limit(1).get();

    if (serviceSnapshot.docs.isNotEmpty) {
      _serviceCollection
          .doc(toSnakeCase(serviceSnapshot.docs.first.id))
          .delete()
          .onError((e, _) => print("Error delete Service document: $e"));
    }
  }
}
