import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:work_schedule/data/models/employee.dart';
import 'package:work_schedule/data/models/model.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/mixins/soft_delete_firestore.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/work_schedule_firestore_collection.dart';
import 'package:work_schedule/data/repository/employee_repository.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';

class EmployeeCfsCollection extends WorkScheduleFirestoreCollection {
  static String get collectionName => 'employees';

  final CollectionReference<Map<String, dynamic>> _employeeCollection =
      FirebaseFirestore.instance.collection(collectionName);

  List<Map<String, dynamic>> _getEmployeesCollection() {
    return [
      {
        EmployeeRepository.columnId: 1,
        EmployeeRepository.columnName: 'Ann',
        EmployeeRepository.columnSpecialtyId: 1,
        SpecialtyRepository().joinAlias: {
          SpecialtyRepository.columnId: 1,
          SpecialtyRepository.columnName: 'Beautician',
        }
      },
      {
        EmployeeRepository.columnId: 2,
        EmployeeRepository.columnName: 'Lisa',
        EmployeeRepository.columnSpecialtyId: 2,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 2, SpecialtyRepository.columnName: 'Manicure'}
      },
      {
        EmployeeRepository.columnId: 3,
        EmployeeRepository.columnName: 'Sony',
        EmployeeRepository.columnSpecialtyId: 3,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 3, SpecialtyRepository.columnName: 'Masseur'}
      },
      {
        EmployeeRepository.columnId: 4,
        EmployeeRepository.columnName: 'Ann2',
        EmployeeRepository.columnSpecialtyId: 3,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 3, SpecialtyRepository.columnName: 'Masseur'}
      },
      {
        EmployeeRepository.columnId: 5,
        EmployeeRepository.columnName: 'Lisa2',
        EmployeeRepository.columnSpecialtyId: 1,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 1, SpecialtyRepository.columnName: 'Beautician'}
      },
      {
        EmployeeRepository.columnId: 6,
        EmployeeRepository.columnName: 'Sony2',
        EmployeeRepository.columnSpecialtyId: 2,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 2, SpecialtyRepository.columnName: 'Manicure'}
      },
      {
        EmployeeRepository.columnId: 7,
        EmployeeRepository.columnName: 'Ann3',
        EmployeeRepository.columnSpecialtyId: 2,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 2, SpecialtyRepository.columnName: 'Manicure'}
      },
      {
        EmployeeRepository.columnId: 8,
        EmployeeRepository.columnName: 'Lisa3',
        EmployeeRepository.columnSpecialtyId: 3,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 3, SpecialtyRepository.columnName: 'Masseur'}
      },
      {
        EmployeeRepository.columnId: 9,
        EmployeeRepository.columnName: 'Sony3',
        EmployeeRepository.columnSpecialtyId: 1,
        SpecialtyRepository().joinAlias: {SpecialtyRepository.columnId: 1, SpecialtyRepository.columnName: 'Beautician'}
      }
    ];
  }

  Future<void> fillEmployees() async {
    final QuerySnapshot<Map<String, dynamic>> query = await _employeeCollection.get();

    if (query.docs.isEmpty) {
      Set<Employee> employees = {};
      _getEmployeesCollection().forEach((Map<String, dynamic> employeeMap) {
        employees.add(Employee.fromNoSqlWithSpecialty(employeeMap));
      });

      await insertBatchEmployees(employees);
    }
  }

  Future<int> insertEmployee(Employee employee) async {
    await _employeeCollection
        .doc(toSnakeCase(employee.name))
        .set(mergeDefaultDeleteAtColumn(employee.toNoSqlWithSpecialty()))
        .onError((e, _) => print("Error inserting Employee document: $e"));

    List<Employee> employees =
        await getStreamOfEmployees(name: employee.name, specialtyId: employee.specialty.id, limitIn: 1)
            .firstWhere((employees) => employees.isNotEmpty && employees.first.id > Model.idForCreating)
            .timeout(const Duration(seconds: 20));

    return employees.first.id;
  }

  Future<void> insertBatchEmployees(Set<Employee> employees) async {
    final batch = FirebaseFirestore.instance.batch();

    for (Employee employee in employees) {
      DocumentReference employeeRef = _employeeCollection.doc(toSnakeCase(employee.name));
      batch.set(employeeRef, mergeDefaultDeleteAtColumn(employee.toNoSqlWithSpecialty()));
    }

    batch.commit().then((_) {});
  }

  Query<Map<String, dynamic>> prepareEmployeeCollectionQuery(
      {String? name, int? specialtyId, int? limitIn = 100, String? orderByIn = 'id DESC'}) {
    Query<Map<String, dynamic>> employeeCollection = _employeeCollection;
    employeeCollection = onWhereNotDeleted(employeeCollection);
    employeeCollection = employeeCollection.where(EmployeeRepository.columnId, isGreaterThan: Model.idForCreating);

    if (name != '') {
      employeeCollection = employeeCollection.where(EmployeeRepository.columnName, isEqualTo: name);
    }

    if (specialtyId != null && specialtyId > Model.idForCreating) {
      employeeCollection = employeeCollection
          .where('${SpecialtyRepository().joinAlias}.${SpecialtyRepository.columnId}', isEqualTo: specialtyId);
    }

    if (limitIn != null) {
      employeeCollection = employeeCollection.limit(limitIn);
    }

    if (orderByIn != null) {
      List<String> orderByValues = orderByIn.split(' ');
      String orderBy = orderByValues[0];
      bool orderDescending = orderByValues[1].toLowerCase() == 'desc';
      employeeCollection = employeeCollection.orderBy(orderBy, descending: orderDescending);
    }

    return employeeCollection;
  }

  Future<List<Employee>> getEmployees({String? name, int? limitIn = 100, String? orderByIn = 'id DESC'}) async {
    Query<Map<String, dynamic>> employeeCollection =
        prepareEmployeeCollectionQuery(name: name, limitIn: limitIn, orderByIn: orderByIn);

    return await employeeCollection.get().then((QuerySnapshot<Map<String, dynamic>> employeeSnapshot) =>
        employeeSnapshot.docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> employeeDocumentSnapshot) =>
                Employee.fromNoSqlWithSpecialty(employeeDocumentSnapshot.data()))
            .toList());
  }

  Stream<List<Employee>> getStreamOfEmployees(
      {String? name, int? specialtyId, int? limitIn = 100, String? orderByIn = 'id DESC'}) async* {
    Query<Map<String, dynamic>> employeeStreamCollection =
        prepareEmployeeCollectionQuery(name: name, specialtyId: specialtyId, limitIn: limitIn, orderByIn: orderByIn);

    Stream<QuerySnapshot<Map<String, dynamic>>> employeeSnapshots = employeeStreamCollection.snapshots();

    yield* employeeSnapshots.map((QuerySnapshot<Map<String, dynamic>> employeeSnapshot) => employeeSnapshot.docs
        .map((QueryDocumentSnapshot<Map<String, dynamic>> employeeDocumentSnapshot) =>
            Employee.fromNoSqlWithSpecialty(employeeDocumentSnapshot.data()))
        .toList());
  }

  Future<Employee> getEmployee(int employeeId) async {
    Query<Map<String, dynamic>> employeeCollection = prepareEmployeeCollectionQuery(limitIn: 1, orderByIn: null);
    employeeCollection = employeeCollection.where('id', isEqualTo: employeeId);

    return await employeeCollection.get().then((QuerySnapshot<Map<String, dynamic>> employeeSnapshot) =>
        employeeSnapshot.docs
            .map((QueryDocumentSnapshot<Map<String, dynamic>> employeeDocumentSnapshot) =>
                Employee.fromNoSqlWithSpecialty(employeeDocumentSnapshot.data()))
            .toList()
            .first);
  }

  Future<int> updateEmployee(Employee employee) async {
    final String docId = await _employeeCollection
        .where(EmployeeRepository.columnId, isEqualTo: employee.id)
        .get()
        .then((employeeSnapshot) => employeeSnapshot.docs.first.id);
    _employeeCollection
        .doc(docId)
        .update(employee.toNoSqlWithSpecialty())
        .onError((e, _) => print("Error update Employee document: $e"));

    return employee.id;
  }

  Future<void> softDeleteEmployee(int employeeId) async {
    final QuerySnapshot employeeSnapshot =
        await _employeeCollection.where(EmployeeRepository.columnId, isEqualTo: employeeId).limit(1).get();

    if (employeeSnapshot.docs.isNotEmpty) {
      _employeeCollection
          .doc(employeeSnapshot.docs.first.id)
          .update({SoftDeleteFirestore.columnDeletedAt: DateTime.now()}).onError(
              (e, _) => print("Error soft delete Employee document: $e"));
    }
  }

  deleteEmployee(int employeeId) async {
    final QuerySnapshot employeeSnapshot =
        await _employeeCollection.where(EmployeeRepository.columnId, isEqualTo: employeeId).limit(1).get();

    if (employeeSnapshot.docs.isNotEmpty) {
      _employeeCollection
          .doc(employeeSnapshot.docs.first.id)
          .delete()
          .onError((e, _) => print("Error delete Employee document: $e"));
    }
  }
}
