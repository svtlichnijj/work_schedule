import 'dart:async';

import 'package:work_schedule/data/models/employee.dart';
import 'package:work_schedule/data/repository/employee_repository.dart';
import 'package:work_schedule/enums/crud_menu_items.dart';

class EmployeeBloc {
  static final _employeeRepository = EmployeeRepository();

  StreamController<List<Employee>> _employeeController = StreamController<List<Employee>>.broadcast();
  late StreamSubscription<List<Employee>> streamSubscription;

  Stream<List<Employee>> _getStreamOfEmployees({String? name}) {
    return _employeeRepository.getStreamOfEmployees(name: name).asBroadcastStream();
  }

  Stream<List<Employee>> get employees => _employeeController.stream;

  EmployeeBloc() {
    getEmployees();
  }

  Future<void> getEmployees({String? nameLike}) async {
    if (!await _getStreamOfEmployees(name: nameLike).isEmpty) {
      streamSubscription = _getStreamOfEmployees(name: nameLike).listen((listOfEmployeesEvent) {
        if (_employeeController.isClosed) {
          _employeeController = StreamController<List<Employee>>.broadcast();
        }

        _employeeController.sink.add(listOfEmployeesEvent);
      }, onError: (e, _) => print("Error adding listen of Stream of Employees to Employee controller: $e"));

      return;
    }

    if (_employeeController.isClosed) {
      _employeeController = StreamController<List<Employee>>.broadcast();
    }

    _employeeController.sink.add(await _employeeRepository.getAllEmployees(nameLike: nameLike));
  }

  void addEmployee(Employee employee) async {
    await _employeeRepository.createEmployee(employee);
    getEmployees();
  }

  void updateEmployee(Employee employee) async {
    await _employeeRepository.updateEmployee(employee);
    getEmployees();
  }

  void softDeleteEmployee(int id) async {
    await _employeeRepository.softDeleteEmployee(id);
    getEmployees();
  }

  void deleteEmployee(int id) async {
    await _employeeRepository.deleteEmployee(id);
    getEmployees();
  }

  Future<void> runMethod(CrudMenuItems crudMenuItem, Iterable<Object?>? positionalArguments) async {
    switch (crudMenuItem) {
      case CrudMenuItems.create:
        addEmployee(positionalArguments!.first as Employee);
        break;
      case CrudMenuItems.read:
        getEmployees(nameLike: positionalArguments!.first as String);
        break;
      case CrudMenuItems.edit:
        updateEmployee(positionalArguments!.first as Employee);
        break;
      case CrudMenuItems.delete:
        softDeleteEmployee(positionalArguments!.first as int);
        break;
      default:
        throw ArgumentError.value(crudMenuItem, 'name');
    }
  }

  dispose() {
    _employeeController.close();
  }
}
