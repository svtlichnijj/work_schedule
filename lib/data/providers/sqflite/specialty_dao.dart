import 'package:sqflite/sqflite.dart';

import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/data/providers/sqflite/mixins/soft_delete_sql.dart';
import 'package:work_schedule/data/providers/sqflite/work_schedule_sqflite_dao.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';

class SpecialtyDao extends WorkScheduleSqfliteDao {
  @override
  String get tableName => 'specialties_table';

  static final SpecialtyDao _instance = SpecialtyDao._();

  factory SpecialtyDao() {
    return _instance;
  }

  SpecialtyDao._();

  List<Map<String, dynamic>> _getSpecialtiesMap() {
    return [
      {SpecialtyRepository.columnId: 1, SpecialtyRepository.columnName: 'Beautician'},
      {SpecialtyRepository.columnId: 2, SpecialtyRepository.columnName: 'Manicure'},
      {SpecialtyRepository.columnId: 3, SpecialtyRepository.columnName: 'Masseur'},
    ];
  }

  Future createTable(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableName (
        ${SpecialtyRepository.columnId} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${SpecialtyRepository.columnName} TEXT NOT NULL UNIQUE,
        $rowCreateSoftDeleteColumn
      );
    ''');

    await fillSpecialties(databaseIn: db);
  }

  Future<List<List<Object?>>> fillSpecialties({Database? databaseIn}) async {
    Set<Specialty> specialties = {};
    _getSpecialtiesMap().forEach((Map<String, dynamic> employeeMap) {
      specialties.add(Specialty.fromMap(employeeMap));
    });
    List<List<Object?>> result = await insertBatchSpecialties(specialties, databaseIn: databaseIn);

    return result;
  }

  Future<int> insertSpecialty(Specialty specialty) async {
    Database db = await _instance.database;
    return await db.insert(tableName, specialty.toMap());
  }

  Future<List<List<Object?>>> insertBatchSpecialties(Set<Specialty> specialties, {Database? databaseIn}) async {
    Database db = databaseIn ?? await _instance.database;
    List<List<Object?>> results = [];
    db.transaction((txn) async {
      Batch batch = txn.batch();
      for (Specialty specialty in specialties) {
        batch.insert(tableName, specialty.toMap());
      }

      results.add(await batch.commit(noResult: false));
    });

    return results;
  }

  Future<List<Specialty>> getSpecialties({String? nameLike, int? limitIn = 100, String? orderByIn = 'id DESC'}) async {
    Database db = await _instance.database;
    final List<Map<String, dynamic>> result;

    if (nameLike != null && nameLike.isNotEmpty) {
      result = await db.query(
        tableName,
        where: '${SoftDeleteSql.onWhereNotDeleted()} AND ${SpecialtyRepository.columnName} LIKE ?',
        whereArgs: ["%$nameLike%"],
        limit: limitIn,
        orderBy: orderByIn,
      );
    } else {
      result = await db.query(
        tableName,
        where: SoftDeleteSql.onWhereNotDeleted(),
        limit: limitIn,
        orderBy: orderByIn,
      );
    }

    return List.generate(result.length, (i) => Specialty.fromMap(result[i]));
  }

  Future<Specialty> getSpecialty(int specialtyId) async {
    Database db = await _instance.database;
    final List<Map<String, dynamic>> map = await db.query(
      tableName,
      where: '${SoftDeleteSql.onWhereNotDeleted()} AND ${SpecialtyRepository.columnId} = ?',
      whereArgs: [specialtyId],
    );

    return Specialty.fromMap(map.last);
  }

  Future<Specialty> upsertSpecialty(Specialty specialty) async {
    Database db = await _instance.database;
    Map<String, dynamic> specialtyMap = specialty.toMap();
    int? count = Sqflite.firstIntValue(await db.rawQuery(
      'SELECT COUNT(*) FROM $tableName WHERE id = ?',
      [specialty.id],
    ));

    if (count == 0) {
      specialtyMap[SpecialtyRepository.columnId] = await db.insert(tableName, specialtyMap);
      specialty = Specialty.fromMap(specialtyMap);
    } else {
      await db.update(
        tableName,
        specialtyMap,
        where: 'id = ?',
        whereArgs: [specialty.id],
      );
    }

    return specialty;
  }

  Future<int> updateSpecialty(Specialty specialty) async {
    Database db = await _instance.database;

    return await db.update(
      tableName,
      specialty.toMap(),
      where: '${SpecialtyRepository.columnId} = ?',
      whereArgs: [specialty.id],
    );
  }

  Future<void> softDeleteSpecialty(int specialtyId) async {
    Database db = await _instance.database;
    await db.update(tableName, {SoftDeleteSql.columnDeletedAt: DateTime.now().millisecondsSinceEpoch},
        where: '${SpecialtyRepository.columnId} = ?', whereArgs: [specialtyId]);
  }

  Future<void> deleteSpecialty(int specialtyId) async {
    Database db = await _instance.database;
    await db.delete(tableName, where: '${SpecialtyRepository.columnId} = ?', whereArgs: [specialtyId]);
  }
}
