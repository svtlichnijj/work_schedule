import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:work_schedule/data/providers/firebase/cloud_firestore/work_schedule_firestore_collection.dart';
import 'package:work_schedule/firebase_options.dart';
import 'package:work_schedule/navigation/navigation.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MainApp({Key? key}) : super(key: key);
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform,)
      .then((FirebaseApp fbApp) async {
    await WorkScheduleFirestoreCollection().onCreate();

    return fbApp;
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // debugShowCheckedModeBanner: false,
      home: FutureBuilder(
          future: _fbApp,
          builder: (BuildContext context, AsyncSnapshot<FirebaseApp> snapshot) {
            if (snapshot.hasError) {
              print('You have an error! ${snapshot.error.toString()}');
              return Text('Something went wrong with FirebaseApp! Error: ${snapshot.error.toString()}');
            } else if (snapshot.hasData) {
              return const Navigation();
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }
      ),
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.brown,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStatePropertyAll<Color>(Theme.of(context).colorScheme.inversePrimary),
            backgroundColor: const MaterialStatePropertyAll<Color>(Colors.brown),
          ),
        ),
        tabBarTheme: const TabBarTheme(
          labelColor: Colors.brown,
        ),
      ),
    );
  }
}
