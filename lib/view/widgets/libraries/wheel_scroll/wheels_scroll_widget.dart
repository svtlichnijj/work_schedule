library wheel_scroll;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:work_schedule/view/widgets/libraries/wheel_scroll/id_scroll_notification.dart';
import 'package:work_schedule/view/widgets/libraries/wheel_scroll/return_type_enum.dart';
import 'package:work_schedule/view/widgets/libraries/wheel_scroll/wheel_seconds.dart';
import 'package:work_schedule/view/widgets/libraries/wheel_scroll/wheel_widget.dart';

import './wheel_am_pm.dart';
import './wheel_hours.dart';
import './wheel_minutes.dart';

class WheelsScrollWidget extends StatefulWidget {
  final ReturnType _returnType;
  final double height;
  final bool isAmPm;
  final bool isShowSeconds;
  final bool isShowCaptions;
  final void Function(Duration) onWheelScrolledDuration;
  final Duration initialTimerDuration;
  final void Function(TimeOfDay) onWheelScrolledTimeOfDay;
  final TimeOfDay initialTimerTimeOfDay;

  static const double defaultHeight = 200.0;

  static durationPickerDefaultCallback(Duration duration) => {};

  static timePickerDefaultCallback(TimeOfDay timeOfDay) => {};

  const WheelsScrollWidget._(this._returnType,
      {
        Key? key,
        this.height = defaultHeight,
        this.isAmPm = false,
        this.isShowSeconds = false,
        this.isShowCaptions = false,
        this.onWheelScrolledDuration = durationPickerDefaultCallback,
        this.initialTimerDuration = const Duration(),
        this.onWheelScrolledTimeOfDay = timePickerDefaultCallback,
        this.initialTimerTimeOfDay = const TimeOfDay(hour: 0, minute: 0),
      }) : super(key: key);

  factory WheelsScrollWidget.callbackDuration({
    Key? key,
    height = defaultHeight,
    isAmPm = false,
    isShowSeconds = false,
    isShowCaptions = false,
    required onWheelScrolledDuration,
    initialTimerDuration,
  }) =>
      WheelsScrollWidget._(
        ReturnType.duration,
        key: key,
        height: height,
        isAmPm: isAmPm,
        isShowSeconds: isShowSeconds,
        isShowCaptions: isShowCaptions,
        onWheelScrolledDuration: onWheelScrolledDuration,
        initialTimerDuration: initialTimerDuration,
      );

  factory WheelsScrollWidget.callbackDateTime({
    Key? key,
    height = defaultHeight,
    isAmPm = false,
    isShowCaptions = false,
    required onWheelScrolledTimeOfDay,
    initialTimerTimeOfDay,
  }) =>
      WheelsScrollWidget._(
        ReturnType.timeOfDay,
        key: key,
        height: height,
        isAmPm: isAmPm,
        isShowSeconds: false,
        isShowCaptions: isShowCaptions,
        onWheelScrolledTimeOfDay: onWheelScrolledTimeOfDay,
        initialTimerTimeOfDay: initialTimerTimeOfDay,
      );

  @override
  State<WheelsScrollWidget> createState() => _WheelsScrollWidgetState();
}

class _WheelsScrollWidgetState extends State<WheelsScrollWidget> {
  FixedExtentScrollController _hourController = FixedExtentScrollController();
  FixedExtentScrollController _minuteController = FixedExtentScrollController();
  FixedExtentScrollController _secondController = FixedExtentScrollController();
  FixedExtentScrollController _amPmController = FixedExtentScrollController();

  static const double _wheelWidth = 40;
  static const double _spacerWidth = WheelWidget.fontSizeBig * 2;

  @override
  void initState() {
    super.initState();

    _hourController = FixedExtentScrollController(initialItem: widget.initialTimerDuration.inHours);
    _minuteController = FixedExtentScrollController(
        initialItem: widget.initialTimerDuration.inMinutes % Duration.minutesPerHour
    );
    _secondController = FixedExtentScrollController(
        initialItem: widget.initialTimerDuration.inSeconds % Duration.secondsPerMinute
    );
    _amPmController = FixedExtentScrollController(
        initialItem: widget.initialTimerDuration.inHours ~/ TimeOfDay.hoursPerPeriod
    );

    if (widget._returnType == ReturnType.duration) {
      WidgetsBinding.instance.addPostFrameCallback((_) => widget.onWheelScrolledDuration(_getDuration()));
    } else if (widget._returnType == ReturnType.timeOfDay) {
      WidgetsBinding.instance.addPostFrameCallback((_) => widget.onWheelScrolledTimeOfDay(_getTimeOfDay()));
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> content = [
      // hours wheel
      SizedBox(
        width: _wheelWidth,
        child: NotificationListener<UserScrollNotification>(
          onNotification: (notification) {
            IdScrollNotification(
              id: 'WheelHours',
              metrics: notification.metrics,
              context: notification.context,
              direction: notification.direction,
            ).dispatch(context);

            return true;
          },
          child: ListWheelScrollView.useDelegate(
            controller: _hourController,
            physics: const FixedExtentScrollPhysics(),
            diameterRatio: 1.2,
            perspective: 0.005,
            useMagnifier: true,
            magnification: 1.3,
            overAndUnderCenterOpacity: 0.5,
            itemExtent: 25,
            childDelegate: ListWheelChildLoopingListDelegate(
                children: WheelHours.getList(isAmPm: widget.isAmPm)
            ),
            onSelectedItemChanged: (int value) {
              _setResult();
            },
          ),
        ),
      ),

      _spacer(data: widget.isShowCaptions ? 'hour' : null),

      // minutes wheel
      SizedBox(
        width: _wheelWidth,
        child: NotificationListener<UserScrollNotification>(
          onNotification: (notification) {
            IdScrollNotification(
              id: 'WheelMinutes',
              metrics: notification.metrics,
              context: notification.context,
              direction: notification.direction,
            )
                .dispatch(context);
            return true;
          },
          child: ListWheelScrollView.useDelegate(
            controller: _minuteController,
            physics: const FixedExtentScrollPhysics(),
            diameterRatio: 1.2,
            perspective: 0.005,
            useMagnifier: true,
            magnification: 1.3,
            overAndUnderCenterOpacity: 0.5,
            itemExtent: 25,
            childDelegate: ListWheelChildLoopingListDelegate(
                children: WheelMinutes.getList()
            ),
            onSelectedItemChanged: (int value) {
              _scrollHoursByMinutes(value);
              _setResult();
            },
          ),
        ),
      ),

      _spacer(data: widget.isShowCaptions ? 'min.' : null),
    ];


    if (widget.isShowSeconds) {
      content.add(
        // seconds wheel
        SizedBox(
          width: _wheelWidth,
          child: NotificationListener<UserScrollNotification>(
            onNotification: (notification) {
              IdScrollNotification(
                id: 'WheelSeconds',
                metrics: notification.metrics,
                context: notification.context,
                direction: notification.direction,
              ).dispatch(context);

              return true;
            },
            child: ListWheelScrollView.useDelegate(
              controller: _secondController,
              physics: const FixedExtentScrollPhysics(),
              diameterRatio: 1.2,
              perspective: 0.005,
              useMagnifier: true,
              magnification: 1.3,
              overAndUnderCenterOpacity: 0.5,
              itemExtent: 25,
              childDelegate: ListWheelChildLoopingListDelegate(
                  children: WheelSeconds.getList()
              ),
              onSelectedItemChanged: (int value) {
                _scrollMinutesBySeconds(value);
                _setResult();
              },
            ),
          ),
        ),
      );

      content.add(_spacer(data: widget.isShowCaptions ? 'sec.' : null));
    }

    if (widget.isAmPm) {
      if (!widget.isShowSeconds) {
        content.add(_spacer());
      }

      content.add(
        // am or pm
        SizedBox(
          width: _wheelWidth,
          child: NotificationListener<UserScrollNotification>(
            onNotification: (notification) {
              IdScrollNotification(
                id: 'WheelAmPm',
                metrics: notification.metrics,
                context: notification.context,
                direction: notification.direction,
              ).dispatch(context);

              return true;
            },
            child: ListWheelScrollView.useDelegate(
              controller: _amPmController,
              physics: const FixedExtentScrollPhysics(),
              diameterRatio: 1.2,
              perspective: 0.005,
              useMagnifier: true,
              magnification: 1.3,
              overAndUnderCenterOpacity: 0.5,
              itemExtent: 25,
              childDelegate: ListWheelChildListDelegate(
                  children: WheelAmPm.getList()
              ),
              onSelectedItemChanged: (int value) {
                _setResult();
              },
            ),
          ),
        ),
      );
    }

    return SizedBox(
      height: widget.height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: content,
      ),
    );
  }

  Widget _spacer({ String? data }) {
    return Center(
      child: SizedBox.square(
        dimension: data == null ? 1 : _spacerWidth,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(4, WheelWidget.fontSizeNormal, 0, 0),
          child: data == null ? null : Text(
            data,
            style: const TextStyle(fontSize: WheelWidget.fontSizeSmall, color: Colors.black54),
            textAlign: TextAlign.start,
          ),
        ),
      ),
    );
  }

  void _setResult() {
    if (widget._returnType == ReturnType.duration) {
      setState(() {
        widget.onWheelScrolledDuration(_getDuration());
      });
    } else if (widget._returnType == ReturnType.timeOfDay) {
      setState(() {
        widget.onWheelScrolledTimeOfDay(_getTimeOfDay());
      });
    }
  }

  void _scrollHoursByMinutes(int minutes, { bool force = false }) {
    final int minutesPerHour = widget._returnType == ReturnType.duration ? Duration.minutesPerHour : TimeOfDay
        .minutesPerHour;
    int offset = 0;
    int itemIndex = _hourController.selectedItem;
    int newValue = minutes % minutesPerHour;

    if (newValue == 59) {
      if (_minuteController.position.userScrollDirection == ScrollDirection.forward || force) {
        offset = -1;
      }
    } else if (newValue == 0) {
      if (_minuteController.position.userScrollDirection == ScrollDirection.reverse || force) {
        offset = 1;
      }
    }

    if (offset != 0) {
      _hourController.animateToItem(
          itemIndex + offset,
          duration: const Duration(seconds: 1),
          curve: Curves.easeInOutSine
      );
    }
  }

  void _scrollMinutesBySeconds(int seconds) {
    int offset = 0;
    int itemIndex = _minuteController.selectedItem;
    int newValue = seconds % Duration.secondsPerMinute;

    if (newValue == 59) {
      if (_secondController.position.userScrollDirection == ScrollDirection.forward) {
        offset = -1;
      }
    } else if (newValue == 0) {
      if (_secondController.position.userScrollDirection == ScrollDirection.reverse) {
        offset = 1;
      }
    }

    if (offset != 0) {
      _minuteController.animateToItem(
          itemIndex + offset,
          duration: const Duration(seconds: 1),
          curve: Curves.easeInOutSine
      ).then((value) => _scrollHoursByMinutes(_minuteController.selectedItem, force: true));
    }
  }

  Duration _getDuration() {
    int hour = _hourController.selectedItem % Duration.hoursPerDay;

    if (widget.isAmPm) {
      hour = hour % 12 + _amPmController.selectedItem == 0 ? 0 : 12;
    }

    return Duration(
      hours: hour,
      minutes: _minuteController.selectedItem % Duration.minutesPerHour,
      seconds: widget.isShowSeconds ? _secondController.selectedItem % Duration.secondsPerMinute : 0,
    );
  }

  TimeOfDay _getTimeOfDay() {
    int hour = _hourController.selectedItem % TimeOfDay.hoursPerDay;

    if (widget.isAmPm) {
      hour = hour % TimeOfDay.hoursPerPeriod + _amPmController.selectedItem == 0 ? 0 : TimeOfDay.hoursPerPeriod;
    }

    return TimeOfDay(
      hour: hour,
      minute: _minuteController.selectedItem % TimeOfDay.minutesPerHour,
    );
  }

  @override
  void dispose() {
    _hourController.dispose();
    _minuteController.dispose();
    _amPmController.dispose();

    super.dispose();
  }
}
