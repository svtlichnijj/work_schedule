import 'package:flutter/material.dart';

import 'package:work_schedule/bloc/specialty_bloc.dart';
import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/view/widgets/common/edit_text_alert_dialog.dart';
import 'package:work_schedule/view/widgets/specialties_list_widget.dart';

class SpecialtiesListTab extends StatefulWidget {
  const SpecialtiesListTab({Key? key}) : super(key: key);

  @override
  State<SpecialtiesListTab> createState() => _SpecialtiesListTabState();
}

class _SpecialtiesListTabState extends State<SpecialtiesListTab> {
  final SpecialtyBloc _specialtyBloc = SpecialtyBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SpecialtiesListWidget(_specialtyBloc),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          showDialog<String>(
              context: context,
              builder: (BuildContext context) {
                return EditTextAlertDialog(
                    title: 'Create specialty',
                    label: 'Specialty',
                    submitText: 'Create',
                    callback: (text) => _insertSpecialty(text),
                );
              }
          );
        },
      ),
    );
  }

  void _insertSpecialty(String specialtyName) {
    if (specialtyName != '') {
      _specialtyBloc.addSpecialty(Specialty(name: specialtyName));
    }
  }

  @override
  dispose() {
    _specialtyBloc.dispose();
    super.dispose();
  }
}
