import 'package:work_schedule/data/models/employee.dart';
import 'package:work_schedule/data/providers/firebase/cloud_firestore/employee_cfs_collection.dart';
import 'package:work_schedule/data/providers/sqflite/employee_dao.dart';
import 'package:work_schedule/data/repository/mixins/foreign_repository.dart';

class EmployeeRepository with ForeignRepository {
  static const columnId = 'id';
  static const columnName = 'name';
  static const columnSpecialtyId = 'specialty_id';

  @override
  String get joinAlias => 'employee';

  @override
  String get foreignKeyColumnName => columnSpecialtyId;

  final EmployeeDao _employeeDao = EmployeeDao();
  final EmployeeCfsCollection _employeeCfsCollection = EmployeeCfsCollection();

  Stream<List<Employee>> getStreamOfEmployees({String? name}) {
    return _employeeCfsCollection.getStreamOfEmployees(name: name);
  }

  Future<List<Employee>> getAllEmployees({String? nameLike}) async {
    return await _employeeCfsCollection.getEmployees(name: nameLike);
    // return await _employeeDao.fetchEmployeesWithSpecialties(nameLike: nameLike);
  }

  Future<Employee> getEmployee(int employeeId) async {
    return await _employeeCfsCollection.getEmployee(employeeId);
    // return await _employeeDao.getEmployee(employeeId);
  }

  Future<Employee> getEmployeeWithSpecialty(int employeeId) async {
    return await _employeeCfsCollection.getEmployee(employeeId);
    // return await _employeeDao.getEmployeeWithSpecialty(employeeId);
  }

  Future<int> createEmployee(Employee employee) async {
    return await _employeeCfsCollection.insertEmployee(employee);
    // return await _employeeDao.insertEmployee(employee);
  }

  Future<int> updateEmployee(Employee employee) async {
    return await _employeeCfsCollection.updateEmployee(employee);
    // return await _employeeDao.updateEmployee(employee);
  }

  Future<void> softDeleteEmployee(int employeeId) async {
    return await _employeeCfsCollection.softDeleteEmployee(employeeId);
    // return await _employeeDao.softDeleteEmployee(employeeId);
  }

  Future<void> deleteEmployee(int employeeId) async {
    return await _employeeCfsCollection.deleteEmployee(employeeId);
    // return await _employeeDao.deleteEmployee(employeeId);
  }
}
