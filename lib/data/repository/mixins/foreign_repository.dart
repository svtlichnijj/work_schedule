mixin ForeignRepository {
  String get joinAlias => 'alias';
  String get foreignKeyColumnName => 'alias_id';
  String get innerKeyColumnName => 'id';
}