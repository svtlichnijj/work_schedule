import 'package:cloud_firestore/cloud_firestore.dart';

mixin SoftDeleteFirestore {
  static const columnDeletedAt = 'deleted_at';

  Map<String, dynamic> mergeDefaultDeleteAtColumn(Map<String, dynamic> mapOfObject) {
    mapOfObject[columnDeletedAt] = null;

    return mapOfObject;
  }

  Query<Map<String, dynamic>> onWhereNotDeleted(Query<Map<String, dynamic>> serviceQuery) {
    return serviceQuery.where(columnDeletedAt, isNull: true);
  }

  Query<Map<String, dynamic>> onWhereDeleted(Query<Map<String, dynamic>> serviceQuery) {
    return serviceQuery.where(columnDeletedAt, isNull: false);
  }
}
