import 'dart:async';

import 'package:work_schedule/data/models/specialty.dart';
import 'package:work_schedule/data/repository/specialty_repository.dart';
import 'package:work_schedule/enums/crud_menu_items.dart';

class SpecialtyBloc {
  static final _specialtyRepository = SpecialtyRepository();

  StreamController<List<Specialty>> _specialtyController = StreamController<List<Specialty>>.broadcast();
  late StreamSubscription<List<Specialty>> streamSubscription;

  Stream<List<Specialty>> _getStreamOfSpecialties({String? name}) {
    return _specialtyRepository.getStreamOfSpecialties(name: name).asBroadcastStream();
  }

  Stream<List<Specialty>> get specialties => _specialtyController.stream;

  SpecialtyBloc() {
    getSpecialties();
  }

  Future<void> getSpecialties({String? nameLike}) async {
    if (!await _getStreamOfSpecialties(name: nameLike).isEmpty) {
      streamSubscription = _getStreamOfSpecialties(name: nameLike).listen((listOfSpecialtiesEvent) {
        if (_specialtyController.isClosed) {
          _specialtyController = StreamController<List<Specialty>>.broadcast();
        }

        _specialtyController.sink.add(listOfSpecialtiesEvent);
      }, onError: (e, _) => print("Error adding listen of Stream of Specialties to Specialty controller: $e"));

      return;
    }

    if (_specialtyController.isClosed) {
      _specialtyController = StreamController<List<Specialty>>.broadcast();
    }

    _specialtyController.sink.add(await _specialtyRepository.getAllSpecialties(nameLike: nameLike));
  }

  Future<void> addSpecialty(Specialty specialty) async {
    await _specialtyRepository.createSpecialty(specialty);
    getSpecialties();
  }

  Future<void> updateSpecialty(Specialty specialty) async {
    await _specialtyRepository.updateSpecialty(specialty);
    getSpecialties();
  }

  Future<void> softDeleteSpecialty(int id) async {
    await _specialtyRepository.softDeleteSpecialty(id);
    getSpecialties();
  }

  Future<void> deleteSpecialty(int id) async {
    await _specialtyRepository.deleteSpecialty(id);
    getSpecialties();
  }

  Future<void> runMethod(CrudMenuItems crudMenuItem, Iterable<Object?>? positionalArguments) async {
    switch (crudMenuItem) {
      case CrudMenuItems.create:
        addSpecialty(positionalArguments!.first as Specialty);
        break;
      case CrudMenuItems.read:
        getSpecialties(nameLike: positionalArguments!.first as String);
        break;
      case CrudMenuItems.edit:
        updateSpecialty(positionalArguments!.first as Specialty);
        break;
      case CrudMenuItems.delete:
        softDeleteSpecialty(positionalArguments!.first as int);
        break;
      default:
        throw ArgumentError.value(crudMenuItem, 'name');
    }
  }

  dispose() {
    _specialtyController.close();
  }
}
