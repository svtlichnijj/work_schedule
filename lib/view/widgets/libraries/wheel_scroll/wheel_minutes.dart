import 'package:flutter/material.dart';

import './wheel_widget.dart';

class WheelMinutes extends WheelWidget {
  final int _minutes;

  WheelMinutes(this._minutes, { super.key }) : super(_minutes.toString());

  @override
  Widget build(BuildContext context) {
    return WheelWidget(_minutes.toString());
  }

  static List<Widget> getList() {
    return List<Widget>.generate(
        60,
        (int index) => WheelMinutes(index)
    );
  }
}
