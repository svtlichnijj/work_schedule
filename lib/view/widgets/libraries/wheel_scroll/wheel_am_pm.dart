import 'package:flutter/material.dart';

import './wheel_widget.dart';

class WheelAmPm extends WheelWidget {
  final String _amPm;

  const WheelAmPm(this._amPm, { super.key }) : super(_amPm);

  @override
  Widget build(BuildContext context) {
    return WheelWidget(_amPm, fontSize: WheelWidget.fontSizeSmall);
  }

  static List<Widget> getList() {
    return <Widget>[
      const WheelAmPm('AM'),
      const WheelAmPm('PM'),
    ];
  }
}
